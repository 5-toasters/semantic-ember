define('component:semantic-checkbox', function() {
    var SemanticCheckboxComponent = Ember.Component.extend({

        tagName: 'div',
        classNames: ['ui', 'checkbox'],
        classNameBindings: ['value:checked'],

        // The checkbox value (true when checked)
        value: null,

        // The checkbox label
        label: null,

        // Disables the checkbox input
        disabled: null,

        // Sets the checkbox value and triggers events
        click: function() {
            var state = !this.get('value');
            this.set('value', state);
            this.sendAction('onClick');
            
            if(state === true) {
                this.sendAction('onCheck');
            } else {
                this.sendAction('onUncheck');
            }
        }

    });

    SemanticCheckboxComponent[Ember.NAME_KEY] = 'semantic-checkbox';
    return SemanticCheckboxComponent;
});