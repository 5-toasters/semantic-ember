/*
	Semantic Drop Down

	Use this component to build dropdowns from controller data.
*/
define('component:semantic-dropdown', function() {
	var SemanticDropdown = Ember.Component.extend({

		// The prompt text to display in the dropdown before any selections are made.
		label: null,

		// The specific type of this dropdown (can be 'item', 'select', or null which is the default).
		type: null,

		// The icon type to show
		icons: 'dropdown',

		// preIcon is the icon to show before the label.
		preIcon: null,

		// action is the dropdown action to apply when an item is selected.
		action: null,

		// Customize Element
		tagName: 'div',
		classNameBindings: ['dropdownClass'],

		// Handles dropdown initialization via semantic.
		didInsertElement: function() {
			var self = this;
			var options = { silent: true };
			if(!!this.get('action')) options.action = this.get('action');

			options.onChange = function(value, text) {
				self.sendAction('change', value, text);
			};

			this.$().dropdown(options);
		},

		// The specific semantic ui css class for this dropdown type.
		dropdownClass: Ember.computed('type', function() {
			switch(this.get('type')) {
				case 'item': return 'ui dropdown item';
				case 'select': return 'ui selection dropdown';
				default: return 'ui dropdown';
			}
		}),

		// True if this dropdown is an input.
		isInput: Ember.computed('type', function() {
			return this.get('type') === 'select';
		})

	});

	SemanticDropdown[Ember.NAME_KEY] = 'SemanticDropdown';
	return SemanticDropdown;
});