/*
	Semantic Picklist
	
	A dropdown input that renders a list of items.
*/
define('component:semantic-picklist', function() {
	var SemanticPicklist = Ember.Component.extend({
		
		// The list of items that will be iterated over to build the dropdown.
		//   When selected, the value will be an item from this list or null.
		list: null,
		
		// The selected value.
		value: null,

		// The key used to determine what property on list items the value should be set to.
		//   If not specified (the default) the value is set to the list item itself.
		key: null,
		
		// The placeholder text.
		placeholder: null,
		
		// The current state of the dropdown.
		//   Can be "active", "loading", "error", "disabled", or null (default).
		state: null,
		
		// Detemrines whether null can be selected or not.
		showNull: false,
		
		
		// Called when the selected value changes passing in the new value.
		onChange: null,

		// type is the a css class added to the ui component.
		type: null,
		
		
		// Handles dropdown initialization via semantic.
		didInsertElement: function() {
			var self = this;
			
			this.$().dropdown({
				silent: true,
				
				// Sets the value with the new selection
				onChange: function(val) {
					if(self._internalChanged) {
						return;
					}

					var newValue = null;
					if(val != null && val != '') {
						var selectedIndex = parseInt(val, 10);
						if(!isNaN(selectedIndex)) {
							newValue = self.get('list')[selectedIndex];
						}
					}
					newValue = self.getValueFromItem(newValue);
					
					self._internalChanged = true;
					self.set('value', newValue);
					delete self._internalChanged;

					self.sendAction('onChange', newValue);
				}
				
			});

			this.valueChanged();
		},
		
		// The specific semantic ui css class for this dropdown type.
		dropdownClass: Ember.computed('state', 'type', function() {
			var className = 'ui ';
			
			switch(this.get('state')) {
				case 'active': className += 'active '; break;
				case 'loading': className += 'loading '; break;
				case 'error': className += 'error '; break;
				case 'disabled': className += 'disabled '; break;
			}

			if(this.get('type') != null) {
				className += this.get('type') + ' ';
			}
			
			className += 'selection dropdown';
			return className;
		}),

		// Updates the UI if the value is changed
		valueChanged: function() {
			if(this._internalChanged === true) {
				return;
			}
			var newValue = this.get('value');

			this._internalChanged = true;
			if(Ember.isNone(newValue)) {
				this.$().dropdown('clear');
			} else {
				var searchValue = newValue,
					list = this.get('list');

				if(list != null && typeof list.indexOf === 'function') {
					var key = this.get('key');

					if(!!key) {
						searchValue = list.findBy(key, newValue);
					}

					this.$().dropdown('set selected', '' + list.indexOf(searchValue));
				}
				
			}
			delete this._internalChanged;
			

		}.observes('value'),

		// getValueFromItem returns the value to set from the selected item.
		getValueFromItem: function(item) {
			var key = this.get('key');

			if(item == null) return item;
			if(!key) return item;
			
			return Ember.get(item, key);
		},
		
		// Customize Element
		tagName: 'div',
		classNameBindings: ['dropdownClass']
		
	});

	SemanticPicklist[Ember.NAME_KEY] = 'SemanticPicklist';
	return SemanticPicklist;
});