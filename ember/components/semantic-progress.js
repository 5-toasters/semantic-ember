/*
	Semantic Progress

	This module displays a Semantic UI progress bar.
*/
define('component:semantic-progress', function() {
	return Ember.Component.extend({

		// label is the text to display under the progress.
		label: null,

		// color is the color of this progress bar.
		color: null,

		// size is the size of the progress.
		size: null,

		// dropdownClass is the combined CSS class names for this progress.
		dropdownClass: Ember.computed('color', function() {
			var color = this.get('color') || '';
			var size = this.get('size') || '';

			return 'ui ' + size + ' ' + color + ' progress';
		}),

		// Tag Customization
		tagName: 'div',
		classNameBindings: ['dropdownClass']

	});
});